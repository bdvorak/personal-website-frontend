const navPages = {
  Home: "/",
  Photography: "/photography"
  // Projects: "/projects",
  // CV: "/cv"
};

export default navPages;
