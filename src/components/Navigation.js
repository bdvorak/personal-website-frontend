import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = ({ pages }) => {
  return (
    <ul className="nav-list">
      {Object.keys(pages).map((pageName, i) =>
        <li key={i} className="item">
          <NavLink to={pages[pageName]} exact={true} activeClassName="active">
            {pageName}
          </NavLink>
        </li>
      )}
    </ul>
  );
};

export default Navigation;
