import React from "react";
import { Link } from "react-router-dom";

import navPages from "../constants/navPages";

import Navigation from "./Navigation";

const Header = () => {
  return (
    <div className="header">
      <Navigation pages={navPages} />
      <Link className="name" to={navPages.Home}>
        Ben Dvorak
      </Link>
    </div>
  );
};

export default Header;
