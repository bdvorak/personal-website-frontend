import React from "react";

const IndexPage = () => {
  return (
    <div className="index-page">
      <div className="row">
        <div className="col-sm-3" />
        <div className="bio col-sm-6">
          Ben is an amateur photographer, computer science student, and
          infrequent rock climber based out of Stanford, California.
        </div>
      </div>
    </div>
  );
};

export default IndexPage;
