import React from "react";
import Photo from "./Photo";
import photos from "../data/photos";
import masonry from "masonry-layout";

class PhotoGrid extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      imageLoadCount: Object.keys(photos).length
    };
  }

  imageLoaded = () => {
    let count = this.state.imageLoadCount;
    if (--count === 0) {
      console.log("loaded");
      new masonry(".grid", {
        columnWidth: ".grid-sizer",
        gutter: ".gutter-sizer",
        itemSelector: ".grid-item",
        percentPosition: true,
        transitionDuration: 0
      });
    }
    this.setState({
      imageLoadCount: count
    });
  };

  render() {
    photos.sort((a, b) => a.id - b.id);
    return (
      <div className="photo-grid grid">
        {photos.map(photo =>
          <div className="grid-item" key={photo.id}>
            <Photo
              {...photo}
              className="grid-photo"
              onLoad={this.imageLoaded}
            />
          </div>
        )}
        <div className="grid-sizer" />
        <div className="gutter-sizer" />
      </div>
    );
  }
}

export default PhotoGrid;
