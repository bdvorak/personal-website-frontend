import React from "react";
import { Switch, Route } from "react-router-dom";

import IndexPage from "./IndexPage";
import PhotoGrid from "./PhotoGrid";
import SinglePhoto from "./SinglePhoto";
import NotFoundPage from "./NotFoundPage";

const Routes = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={IndexPage} />
        <Route exact path="/photography" component={PhotoGrid} />
        <Route path="/photography/:photoId" component={SinglePhoto} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
};

export default Routes;
