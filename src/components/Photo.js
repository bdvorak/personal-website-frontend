import React from "react";
import { Link } from "react-router-dom";
// import CSSTransitionGroup from 'react-addons-css-transition-group';

const Photo = ({ className, id, name, src, location, orientation, type, onLoad }) => {
  return (
    <div className={`${orientation}`}>
      <Link to={`/photography/${id}`}>
        <img
          src={src}
          alt={name}
          onLoad={onLoad}
          onError={onLoad}
          className={className}
        />
      </Link>

      {/*<CSSTransitionGroup transitionName="like" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
            <span key={post.likes} className="likes-heart">{post.likes}</span>
          </CSSTransitionGroup>*/}
    </div>
  );
};

export default Photo;
