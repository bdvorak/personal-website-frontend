import React from "react";
import { BrowserRouter } from "react-router-dom";

import Header from "./Header";
import Routes from "./Routes";

import "../static/style.scss";

const App = () => {
  return (
    <BrowserRouter>
      <div className="app container-fluid">
        <Header />
        <div className="content">
          <Routes />
        </div>
      </div>
    </BrowserRouter>
  );
};

export default App;
