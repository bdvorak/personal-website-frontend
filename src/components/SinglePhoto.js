import React from "react";

import photos from "../data/photos";
import Photo from "./Photo";
import NotFoundPage from "./NotFoundPage";

const SinglePhoto = ({ match }) => {
  let photo = photos.filter(p => p.id === match.params.photoId)[0];
  if (typeof photo === "undefined") {
    return <NotFoundPage />;
  }
  return (
    <div className="single-photo">
      <Photo {...photo} className="single" />
    </div>
  );
};

export default SinglePhoto;
