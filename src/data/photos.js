const photos = [
  {
    id: "1",
    name: "Vaillancourt Fountain",
    location: "San Francisco",
    type: "35",
    orientation: "portrait",
    src:
      "https://so4vbg.bn1304.livefilestore.com/y4mQAJvfbzrk4QLykAHym3z47-6rQ2k8d4pWmuibmrFaDODhvdW5cs3DJEvLgl5VpIknpLb3T9hjpesJTmuUucSGqkFP5j0OoObsM7A7w_5ERkVLHvXc6CO1pF3Pp24_5pyypbNEIWTlMVpifUxhwHIMhLZYOs2mRYWJP_FoPB0yOeaemqFwB2o-EhkX424blglUGAu3nOs9eWnjGnRWZFKSw?width=677&height=1024&cropmode=none",
    instalink: "https://www.instagram.com/p/BU_GpO5F3Mj/"
  },
  {
    id: "3",
    name: "SFFD",
    location: "San Francisco",
    type: "35",
    orientation: "portrait",
    src:
      "https://sy4sbg.bn1304.livefilestore.com/y4mG5Ze7XbacmgJNBWoahDFt9eUfISJ8AAN1RC4QqO5Vkaw7JWYytihSxEydVjQ8FucE2selmbehy7Ybm-ykcZ1zrgC5qQOazs20gqjnuSWaXf2RUNET4ZvV_t0U9leX2KQhVJI2UevlQJ_1btQw3b6JpWu5leOJuLRiH3FT5qSVQPDtRgOB6y5DclDotWyzwWrwGWIzZBhdopWxlb1AC2Q5g?width=685&height=1024&cropmode=none",
    instalink: "https://www.instagram.com/p/BVsN5LVhTH_/"
  },
  {
    id: "2",
    name: "Corner Building",
    location: "San Francisco",
    type: "35",
    orientation: "landscape",
    src:
      "https://sy4xbg.bn1304.livefilestore.com/y4m731wH1ADsNE9lfiWmEDUuuLcfcaRCBR2KL1lp6oHbe92S_7QujnW4eLo57GNAFl9bGie1fzdEsQ6XuMI3UC3tVtctYS_cnOrM_Jj6Se1TLlBZqqcjOlaZTqDOwb6k_J44a_SnQgWghHqpWRl5smdfOe9QzN_IJrq0W8y_p5qYtT9nT28aJWWurhegzhh4MA-56sCwY1t4kCNAk6CCuQ9fw?width=1024&height=700&cropmode=none",
    instalink: "https://www.instagram.com/p/BVsN5LVhTH_/"
  },
  {
    id: "4",
    name: "Hospital",
    type: "35",
    orientation: "landscape",
    location: "San Francisco",
    src:
      "https://sy4ubg.bn1304.livefilestore.com/y4mzduLUa_NM-bQgH1MYAwS4AaMFji5Xty3-oKi6ryUZx9JqU9rcRdlH7sm4JvA6TuFZDCTR-056F-nI_B6wTaN4pA0bnxqke-GDMZWCzNhcolvOLBAEyhY7JldHlcX3kOLrbcaEDGhpGJpNoM51WZm7-gPK5R_y3iOOBgAGLlH70ournKpXEFDMoLi2hM7ASpwwb3q4iR0JGkzIJ-rOHa5VA?width=1024&height=674&cropmode=none",
    instalink: "https://www.instagram.com/p/BVGUGnMh3uY/"
  },
  {
    id: "5",
    name: "McMurtry Bench",
    type: "35",
    orientation: "landscape",
    location: "Stanford",
    src:
      "https://uo7i4g.bn1304.livefilestore.com/y4mvdxV6Nxuvs4i_a34gHGMF9rctIBft7w--t_Mffho3PXrIgdrBAP34pE0vFshou_lR6H8D3qDluo2YiIIb48tUW7TFZn8Say7vUTvdryr_46yQML2FAagSOK5yRWPI_t5QWuCC9olVGtDeHLNoi3itge8v6vrQceSipBh7ePfxJ0Nan20PCraLD8h67JVuXxsj__dKQ-23YCisQTN0VmGVw?width=1024&height=675&cropmode=none",
    instalink: "https://www.instagram.com/p/BWkI4Qhh5zT/"
  },
  {
    id: "6",
    name: "Playset",
    type: "35",
    orientation: "landscape",
    location: "San Francisco",
    src:
      "https://sy4qbg.bn1304.livefilestore.com/y4moIbKTJyyRZ57gjM-06FJxykP1eQkioStmU5s4oFhlatcoNsGn--tpjjuwJvMqOHuvHplX_PUJjLFnOm4HpWjgK1spHI2SxNgGel2lZPinzjV4HYM1_pwwR0vKIlxZxFtjFt3wGM3GCZbeHR860m-FfMVVY-OQNvZJKMaMg18eExATrYTyHhciaxVUywS3Ltm_zaNce4-BUNtg5wiM-s0vw?width=1024&height=677&cropmode=none",
    instalink: null
  },
  {
    id: "7",
    name: "Books",
    type: "35",
    orientation: "portrait",
    location: null,
    src:
      "https://sy4lbg.bn1304.livefilestore.com/y4m_dB2sk9D4PBJzx9KvqWnIIP9xI1fW6OSl_j7B62J6Kufxs0pcy_po3aojKMANBlVk6yuav6LNiZ-Pg6795khQjSiUvOmS2Nt66tAjXkrC1Tz6-Z2TzJu3qTUb6i0lm1X4H6Gb0jo0b3MAcHLwOscmn3uAjnajscgqASHO9mxsfL1dNqJZKX2uKi7FiITeq6SXgJGNiMpsgUhN3qJTyABlQ?width=671&height=1024&cropmode=none",
    instalink: null
  },
  {
    id: "8",
    name: "McMurtry",
    type: "35",
    orientation: "landscape",
    location: "Stanford",
    src:
      "https://uo7h4g.bn1304.livefilestore.com/y4m3UQSSJ6Rd9A3nWmVSY03rhMjsiCnMRV5H5bIUsbgTJPoAMcd66MPQzx1vZ36M8KUeUijJH83N9szlMiOaKQ0sO4NixTgIFYB07U7Ur6rVmcDeauPe_j2OLVe3McgbFxaTQuSpNKj1QaVyb4qUHv7o6cdISL0Yb48OBefc4AZRXBYkOXEE0JodhlNQ8qVexZQODnJahW69O7f2OEOaxCt-A?width=1024&height=672&cropmode=none",
    instalink: null
  }
];

export default photos;
